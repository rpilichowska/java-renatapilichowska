import java.util.*;

public class Zad2d
{
    public static void main(String[] args)
    {
        System.out.println("Podaj n z zakresu <1,100>");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if(n < 1 || n > 100)
        {
            System.out.println("N nie miesci sie w zakresie");
            System.exit(0);
        }
        int tablica[] = new int[n];
        generuj(tablica,n,-999,999);
        for(int i = 0 ; i < tablica.length; i++){
            System.out.print(tablica[i] + " ");
        }
        System.out.println("");
        System.out.println("Suma Dodatnich : " + sumaDodatnich(tablica));
        System.out.println("Suma Ujemnych : " + sumaUjemnych(tablica));


    }
    public static void generuj(int tab[],int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for(int i = 0 ; i < n; i++){
            tab[i] = r.nextInt(maxWartosc-minWartosc) + minWartosc;
        }
    }


    public static int sumaDodatnich(int tab[]){
        int suma = 0;
        for(int element: tab){
            if(element > 0){
                suma+=element;
            }
        }
        return suma;
    }

    public static int sumaUjemnych(int tab[]){
        int suma = 0;
        for(int element: tab){
            if(element < 0){
                suma+=element;
            }
        }
        return suma;
    }


}

