import java.util.*;

public class Zad1g
{
    public static void main(String[] args)
    {
        System.out.println("Podaj n z zakresu <1,100>");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if(n < 1 || n > 100)
        {
            System.out.println("N nie miesci sie w zakresie");
            System.exit(0);
        }
        System.out.println("Podaj lewy z zakresu <1,n)");
        int lewy = in.nextInt();
        System.out.println("Podaj prawy z zakresu <1,n)");
        int prawy = in.nextInt();
        int tablica[] = new int[n];
        Random r = new Random();
        for(int j = 0 ; j < tablica.length ;j++){
            tablica[j] = r.nextInt(2000) - 1000;
        }
        for(int i = 0 ; i < tablica.length; i++){
            System.out.print(tablica[i] + " ");
        }
        System.out.println("");
        for(int i = lewy ; i < prawy; i++){
            int temp = tablica[i-1];
            tablica[i-1] = tablica[prawy-1];
            tablica[prawy-1] = temp;
            prawy--;
        }

        for(int i = 0 ; i < tablica.length; i++){
            System.out.print(tablica[i] + " ");
        }
        System.out.println("");



    }

}

