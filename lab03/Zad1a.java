import java.util.*;

public class Zad1a
{
    public static void main(String[] args)
    {
        System.out.println("Podaj n z zakresu <1,100>");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if(n < 1 || n > 100)
        {
            System.out.println("N nie miesci sie w zakresie");
            System.exit(0);
        }
        int tablica[] = new int[n];
        Random r = new Random();
        for(int j = 0 ; j < tablica.length ;j++){
            tablica[j] = r.nextInt(2000) - 1000;
        }
        for(int i = 0 ; i < tablica.length; i++){
            System.out.print(tablica[i] + " ");
        }
        System.out.println("");

    }
    
    public static void generuj(int tab[],int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for(int i = 0 ; i < n; i++){
            tab[i] = r.nextInt(maxWartosc) - (-minWartosc + maxWartosc);
        }
    }

}

