import java.util.*;

public class Zad2_2
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj liczbe naturalna n: ");
        int n = in.nextInt();
        int j = 0;
        int suma = 0;
        while(j<n){
            System.out.println("Podaj kolejna liczbe: ");
            int liczba = in.nextInt();
            if( liczba > 0){
                suma += liczba;
            }
            j++;
        }
        System.out.println("Suma = : " + 2*suma);
}
}

