import java.util.Scanner;
import java.lang.Math.*;

public class Zad1h {
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Zadanie 1h");

        System.out.println("Podaj ile liczb wpiszesz: ");
        int n = in.nextInt();
        double rozwiazaniedod = 0;
        double liczba;
        for(int i = 0;i < n; i++)
        {
            System.out.println("Podaj liczbe: ");
            liczba = in.nextInt();
            liczba = liczba * Math.pow(-1, i+2);
            rozwiazaniedod = rozwiazaniedod + liczba;
        }
        System.out.println("Twoj wynik dodawania to: " + rozwiazaniedod);
        
    }

}

