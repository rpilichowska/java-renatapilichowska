import java.util.Scanner;
import java.lang.Math.*;

public class Zad1g {
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Zadanie 1g");

        System.out.println("Podaj ile liczb wpiszesz: ");
        int n = in.nextInt();
        double rozwiazaniedod = 0;
        double rozwiazaniemno = 1;
        double liczba;
        for(int i = 0;i < n; i++)
        {
            System.out.println("Podaj liczbe: ");
            liczba = in.nextInt();
            rozwiazaniemno = rozwiazaniemno * liczba;
            rozwiazaniedod = rozwiazaniedod + liczba;
        }
        System.out.println("Twoj wynik dodawania to: " + rozwiazaniedod);
        System.out.println("Twoj wynik mnozenia to: " + rozwiazaniemno);
        
    }

}

