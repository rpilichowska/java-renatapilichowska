package pl.imiajd.pilichowska;

import java.time.LocalDate;
import java.util.Comparator;

public class Student extends Osoba implements Comparable<Osoba>, Cloneable {

	public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
		super(nazwisko, dataUrodzenia);
		this.sredniaOcen = sredniaOcen;
	}
	
	
	public String toString()
	{
		String s = super.toString();
		s = s + " " + this.sredniaOcen;
		
		return s;
	}
	
	
	public int compareTo(Osoba temp)
	{
		Student inna = (Student) temp;
		if(super.compareTo(inna) == 0)
		
		{
			if(((Double)this.getSredniaOcen()).compareTo((Double)inna.getSredniaOcen()) > 0)
			{
				return 1;
			}
			
			else if(((Double)this.getSredniaOcen()).compareTo((Double)inna.getSredniaOcen()) < 0)
			{
				return -1;
			}
			
			
			return 0;
		}
		
		return super.compareTo(inna);
	}
	
	public static Comparator<Student> StudentSredniaComparator = new Comparator<Student>()
			{
				public int compare(Student os1, Student os2)
				{
					Double srednia1 = new Double(os1.getSredniaOcen());
					Double srednia2 = new Double(os2.getSredniaOcen());
					
					return srednia1.compareTo(srednia2);
				}
			};
			
	public Student clone() throws CloneNotSupportedException
	{
		Student cloned = (Student)super.clone();
		cloned.sredniaOcen = this.sredniaOcen;
		
		
		return cloned;
		
	}
	
	public double getSredniaOcen()
	{
		return sredniaOcen;
	}
	
	private double sredniaOcen;

}
