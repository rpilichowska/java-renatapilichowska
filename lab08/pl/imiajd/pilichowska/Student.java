package pl.imiajd.pilichowska;
import java.util.*;
import java.time.LocalDate;

class Student extends Osoba

{
    public Student(String nazwisko, boolean plec, String[] imiona , LocalDate dataUrodzenia, String kierunek,double sredniaOcen)
    {
        super(nazwisko,plec,imiona,dataUrodzenia);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }
    
    public double getSredniaOcen(){
        return sredniaOcen;
    }
    public void setSredniaOcen(double srednia){
        this.sredniaOcen = srednia;
    }


    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }

    private String kierunek;
    private double sredniaOcen;
}

