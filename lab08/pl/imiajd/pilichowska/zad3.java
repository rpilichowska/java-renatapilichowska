import java.util.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class zad3
{
    public static void main(String[] args)
    {
    ArrayList<Instrument> orkiestra = new ArrayList<Instrument>();
    orkiestra.add(0, new Flet("puzon1",LocalDate.now()));
    orkiestra.add(1, new Skrzypce("puzon2",LocalDate.now()));
    orkiestra.add(2, new Skrzypce("puzon3",LocalDate.now()));
    orkiestra.add(3, new Fortepian("puzon4",LocalDate.now()));
    orkiestra.add(4, new Fortepian("puzon5",LocalDate.now()));

    for(int j = 0 ; j< orkiestra.size(); j++){
        orkiestra.get(j).dzwiek();
    }
    }
}


abstract class Instrument
{
    private String producent;
    private LocalDate rokProdukcji;


    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }
    

    public String getProducent(){
        return producent;
    }

    public LocalDate getRokProdukcji(){
        return rokProdukcji;
    }

    public abstract void dzwiek();

    public boolean equals(Instrument other){
        if(this.producent == other.producent && this.rokProdukcji == other.rokProdukcji)
        {
            return true;
        }
        return false;
    }
    
    public String toString(){
        String napis = " ";
        napis += getProducent() + " " + getRokProdukcji();
        return napis;
    }
}


class Flet extends Instrument{
    public Flet(String producent, LocalDate rokProdukcji)
    {
        super(producent, rokProdukcji);
    }


    public void dzwiek(){
        System.out.println("fi , fi, friiiiiii");
    }
}
class Fortepian extends Instrument{
    public Fortepian(String producent, LocalDate rokProdukcji)
    {
        super(producent, rokProdukcji);
    }


    public void dzwiek(){
        System.out.println("Ram pam pam");
    }
}
class Skrzypce extends Instrument{
    public Skrzypce(String producent, LocalDate rokProdukcji)
    {
        super(producent, rokProdukcji);
    }


    public void dzwiek(){
        System.out.println("brzdek brzdek brzdek");
    }
}


