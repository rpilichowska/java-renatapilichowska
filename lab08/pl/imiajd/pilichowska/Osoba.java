package pl.imiajd.pilichowska;
import java.util.*;
import java.time.LocalDate;

abstract class Osoba
{
    public Osoba(String nazwisko, boolean plec, String[] imie, LocalDate dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.plec = plec;
        this.dataUrodzenia = dataUrodzenia;
        imiona = new String[imie.length];
        for(int j = 0 ; j < imie.length ; j++){
            imiona[j] = imie[j];
            
            
        }

    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public boolean getPlec()
    {
        return plec;
    }

    public String[] getImiona(){
        return imiona;
    }
    
    public LocalDate getDataURodzenia(){
        return dataUrodzenia;
    }




    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;

}
