package pl.imiajd.pilichowska;
import java.util.*;
import java.time.LocalDate;

class Pracownik extends Osoba

{
    public Pracownik(String nazwisko, boolean plec , String[] imiona , LocalDate dataUrodzenia, double pobory,LocalDate dataZatrudnienia)
    {
        super(nazwisko,plec,imiona,dataUrodzenia);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }
    

    public double getPobory()
    {
        return pobory;
    }


    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }
    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }


    private double pobory;
    private LocalDate dataZatrudnienia;

}
