import java.lang.*;
import java.util.*;
public class zad5
{
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<>(4);
        ArrayList<Integer> b = new ArrayList<>(5);
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        reverse(a);
        System.out.println("");
        for(int j = 0; j < a.size() ; j ++)
        {
            System.out.print(a.get(j) + " ");
        }
        System.out.println("");
        reverse(b);
        System.out.println("");
        for(int j = 0; j < b.size() ; j ++)
        {
            System.out.print(b.get(j) + " ");
        }
    }

    public static void reverse(ArrayList<Integer> a)
    {
        for(int j = 0,i = a.size() - 1; i > j ; j++,i--)
        {
            int temp = a.get(j);
            a.set(j,a.get(i));
            a.set(i,temp);
        }
        for(int j = 0; j < a.size() ; j ++)
        {
            System.out.print(a.get(j) + " ");
        }
        System.out.println("");

   }
}
