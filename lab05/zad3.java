
import java.util.*;
public class zad3
{
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<>(4);
        ArrayList<Integer> b = new ArrayList<>(5);
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        b.add(4);
        b.add(7);
        b.add(9);
        b.add(9);
        b.add(11);
        ArrayList<Integer> nala = merge(a,b);
        for(int j = 0; j < nala.size() ; j ++)
        {
            System.out.print(nala.get(j) + " ");
        }
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a , ArrayList<Integer> b)
    {
        int rozmiar = a.size() + b.size();
        ArrayList<Integer> nala = new ArrayList<>(rozmiar);
        int max;
        int min;
        ListIterator<Integer> ita = a.listIterator();    
        ListIterator<Integer> itb = b.listIterator();    
        if (a.size() > b.size()) {
            max = a.size();
            min = b.size();
        }

        else {
            max = b.size();
            min = a.size();
        }
        for(int j = 0; j < rozmiar ; j ++)
        {
            boolean la = ita.hasNext();
            boolean lb = itb.hasNext();
            if(la && lb){
                int zmienna1 = ita.next();
                int zmienna2 = itb.next();
                if(zmienna1<zmienna2){
                    nala.add(zmienna1);
                    zmienna2 = itb.previous();
                }
                else{
                    nala.add(zmienna2);
                    zmienna1 = ita.previous();
                }
            }
            if(la && !lb){
                nala.add(ita.next());
            }
            else if(lb && !la){
                nala.add(itb.next());
            }

        }


    return nala;
   }
}

