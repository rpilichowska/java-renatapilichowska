
class PairUtil
{
	public static <T> Pair<T> swap(Pair<T> s)
	{
		Pair<T> tmp = new Pair<>(s.getSecond(), s.getFirst());
		return tmp;
	}
}
