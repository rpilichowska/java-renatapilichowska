import java.time.LocalDate;

public class PairZad3
{
    public static void main(String[] args)
    {
        String[] words = { "Ala", "ma", "psa", "psa"};

		LocalDate[] daty = {LocalDate.of(1992, 10, 10), LocalDate.of(1993, 10, 10), LocalDate.of(1994, 10, 10)};

		Integer[] inty = {10, 20, 30, 40};

        Pair<String> mm = ArrayAlg.minmax(words);
        System.out.println("1 = " + mm.getFirst());
        System.out.println("2 = " + mm.getSecond());

		mm.swap();
        System.out.println("1 = " + mm.getFirst());
        System.out.println("2 = " + mm.getSecond());

		Pair<String> bb = PairUtil.swap(mm);

		System.out.println("1 = " + bb.getFirst());
		System.out.println("2 = " + bb.getSecond());

		System.out.println(ArrayUtil.isSorted(words));
		System.out.println(ArrayUtil.isSorted(daty));
		System.out.println(ArrayUtil.isSorted(inty));

		System.out.println(ArrayUtil.binSearch(inty, 12));
    }
}

class ArrayAlg
{

    public static Pair<String> minmax(String[] a)
    {
        if (a == null || a.length == 0) {
            return null;
        }

        String min = a[0];
        String max = a[0];

        for (int i = 1; i < a.length; i++) {
            if (min.compareTo(a[i]) > 0) {
                min = a[i];
            }

            if (max.compareTo(a[i]) < 0) {
                max = a[i];
            }
        }

        return new Pair<String> (min, max);
    }
}

class ArrayUtil 
{
	public static <T extends Comparable<T>> boolean isSorted(T[] tab)
	{
		T stary = tab[0];
		boolean czy = true;
		for(int j = 0; j < tab.length; j++)
		{
			T nowy = tab[j];
			if(nowy.compareTo(stary) < 0)
			{
				czy = false;
			}
			stary = nowy;
			System.out.println(tab[j]);
		}
		return czy;
	}

	public static <T extends Comparable<T>> int binSearch(T[] tab, T el)
	{
		int sr;
		int l = 0;
		int p = tab.length;
		while(l <= p)
		{
			sr = (l + p) / 2;
			if(tab[sr].compareTo(el) == 0)
			{
				return sr;
			}

			if(tab[sr].compareTo(el) > 0)
			{
				p = sr - 1;
			}
			else
			{
				l = sr + 1;
			}
		}

		return -1;
	}
		
}
