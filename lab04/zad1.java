import java.util.*;

public class zad1
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj znak");
        char userChar = in.next().charAt(0);
        System.out.println("Podaj napis");
        in.nextLine();
        String napis = in.nextLine();
        System.out.println(napis);
        System.out.println("Podany znak wystepuje w podanym napisie " + countChar(napis,userChar) + " razy");

    }

    public static int countChar(String str, char c){
        int liczba = 0;
        for(int i = 0; i < str.length();i++)
        {
            if(str.charAt(i) == c){
                liczba++;
            }
        }
        return liczba;
    }

}

