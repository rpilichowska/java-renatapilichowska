package pl.imiajd.pilichowska;

public class Nauczyciel extends Osoba{
	
    private double pensja;
    

    public Nauczyciel(double pensja,String nazwisko, int rokUrodzenia){
        super(nazwisko,rokUrodzenia);
        this.pensja = pensja;
        
    }
    
    public String toString(){
        String nala = "";
        nala += super.toString();
        nala +=  " ";
        nala += this.pensja;
        return nala;
    }
}

