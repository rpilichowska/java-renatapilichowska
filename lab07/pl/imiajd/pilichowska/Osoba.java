package pl.imiajd.pilichowska;

public class Osoba
{
    private String nazwisko;
    private int rokUrodzenia;
    
    Osoba(String nazwisko, int rokUrodzenia){
		
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
        
    }
    
    public String toString(){
		
        String nala = "";
        nala += this.nazwisko;
        nala += "  ";
        nala += this.rokUrodzenia;
        return nala;
        
    }
}

