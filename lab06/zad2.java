import java.util.*;
public class zad2
{
    public static void main(String[] args)
    {
        IntegerSet zbior1 = new IntegerSet();
        zbior1.setElem(10,true);
        IntegerSet zbior2 = new IntegerSet();
        IntegerSet zbior3 = IntegerSet.union(zbior1,zbior2);
        System.out.println(zbior3.getElem(10));
        System.out.println(zbior3.getElem(1));
        System.out.println(zbior1.toString());

    }

}

class IntegerSet
{
    private boolean[] tablica;
    public IntegerSet()
    {
        tablica = new boolean[100];
        for(int j = 0 ; j < 100; j++)
        {
            tablica[j] = false;
        }
    }
    public void setElem(int indeks , boolean wartosc)
    {
        this.tablica[indeks] = wartosc;
    }
    public boolean getElem(int indeks)
    {
        return tablica[indeks];

    }
    public static IntegerSet union(IntegerSet zbior1 , IntegerSet zbior2)
    {
        IntegerSet nala = new IntegerSet();
        for(int j = 0 ; j < 100 ; j++)
        {
            boolean wartosc1 = zbior1.tablica[j];
            boolean wartosc2 = zbior2.tablica[j];
            if(wartosc1 | wartosc2)
            {
                nala.setElem(j,true);
            }


        }
        return nala;
    }
    public static IntegerSet intersection(IntegerSet zbior1 , IntegerSet zbior2)
    {
        IntegerSet nala = new IntegerSet();
        for(int j = 0 ; j < 100 ; j++)
        {
            boolean wartosc1 = zbior1.tablica[j];
            boolean wartosc2 = zbior2.tablica[j];
            if(wartosc1 && wartosc2)
            {
                nala.setElem(j,true);
            }


        }
        return nala;
    }
    public void insertElement(int liczba)
    {
        this.tablica[liczba] = true;
    }
    public void deleteElement(int liczba)
    {
        this.tablica[liczba] = false;
    }
    
    public String toString()
    {   String nala = "";
        int wartosc = -1;
        for(int j = 0; j < 100 ; j++)
        {
            if(this.tablica[j])
            {
                nala.concat(Integer.toString(j));
            }
        }
        return nala;
    }
    public boolean equals(IntegerSet zbior)
    {
        for(int j = 0; j<99; j++)
        {
            if(this.tablica[j] != zbior.tablica[j])
            {
                return false;
            }
        }
    return true;
    }
}
