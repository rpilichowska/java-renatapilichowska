package pl.imiajd.pilichowska;
import java.util.*;
import java.time.LocalDate;

public class Osoba implements Comparable<Osoba>



{
    public Osoba(String nazwisko,LocalDate dataUrodzenia)
    
    
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public int compareTo(Osoba other){
        if(this.nazwisko.compareTo(other.nazwisko) < 0){
            return -1;
        }
        if(this.nazwisko.compareTo(other.nazwisko)> 0){
            return 1;
        }
        return 0;
    }

    public String toString(){
        String nala = " ";
        nala += this.getClass();
        nala += " [ ";
        nala += this.nazwisko;
        nala += " ";
        nala += this.dataUrodzenia.toString();
        nala += " ]";
        return nala;
    }

    public boolean equals(Osoba other){
        if (this == other ) {
            return true;
        }
        if (!(this.getClass() == other.getClass())){
            return false;
        }
        if(this.nazwisko.equals(other.nazwisko) && this.dataUrodzenia.equals(other.dataUrodzenia)){
            return true;
        }
        return false;
    }


    private String nazwisko;
    private LocalDate dataUrodzenia;
}


